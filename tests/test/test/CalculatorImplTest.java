/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import main.Calculator;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author christopher
 */
public class CalculatorImplTest {

    public CalculatorImplTest() {
    }

    /**
     * Test of multiply method, of class CalculatorImpl.
     */
    /*@Test
    public void testMultiply() {
        System.out.println("multiply");
        int a = 0;
        int b = 0;
        CalculatorImpl instance = new CalculatorImpl();
        int expResult = 0;
        int result = instance.multiply(a, b);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }*/

    /**
     * Test of divide method, of class CalculatorImpl.
     */
    @Test(expected = ArithmeticException.class)
    public final void testDivideByZero() {
        Calculator calc = new CalculatorImpl();
        int a, b, res;
        a = 5;
        b = 0;
        res = 0;
        if (calc.divide(a, b) != res) {
            fail("b nul");
        }
        a = 0;
        b = 0;
        res = 0;
        if (calc.divide(a, b) != res) {
            fail("a et b nuls");
        }
    }

    /**
     * Test of add method, of class CalculatorImpl.
     */
    @Test
    public final void testAdd() {
        Calculator calc = new CalculatorImpl();
        int a, b, res;
        a = 5;
        b = 5;
        res = a + b;
        assertTrue("a et b positif", calc.add(a, b) == res);
        a = 0;
        b = 5;
        res = a + b;
        assertTrue("a nul", calc.add(a, b) == res);
        a = 5;
        b = 0;
        res = a + b;
        assertTrue("b nul", calc.add(a, b) == res);
        a = 0;
        b = 0;
        res = a + b;
        assertTrue("a et b nuls", calc.add(a, b) == res);
        a = -5;
        b = 5;
        res = a + b;
        assertTrue("a negatif", calc.add(a, b) == res);
        a = 5;
        b = -5;
        res = a + b;
        assertTrue("b negatif", calc.add(a, b) == res);
        a = -5;
        b = -5;
        res = a + b;
        assertTrue("a et b negatif", calc.add(a, b) == res);
    }

    /**
     * Test of substract method, of class CalculatorImpl.
     */
    @Test
    public void testSubstract() {
        int a, b, res;

        CalculatorImpl calc = new CalculatorImpl();

        a = 0;
        b = 0;
        res = a - b;
        if (calc.substract(a, b) != res) {
            fail("a et b nuls");
        }

        a = 5;
        b = 5;
        res = a - b;
        if (calc.substract(a, b) != res) {
            fail("a et b positifs");
        }

        a = 0;
        b = 5;
        res = a - b;
        if (calc.substract(a, b) != res) {
            fail("a nul");
        }

        a = 5;
        b = 0;
        res = a - b;
        if (calc.substract(a, b) != res) {
            fail("b nul");
        }

        a = -5;
        b = 5;
        res = a - b;
        if (calc.substract(a, b) != res) {
            fail("a negatif");
        }

        a = 5;
        b = -5;
        res = a - b;
        if (calc.substract(a, b) != res) {
            fail("b negatif");
        }

        a = -5;
        b = -5;
        res = a - b;
        if (calc.substract(a, b) != res) {
            fail("a et b negatifs");
        }
    }

}
