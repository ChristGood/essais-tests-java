/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import main.Calculator;

/**
 *
 * @author christopher
 */
public class CalculatorImpl implements Calculator {

    @Override
    public int multiply(int a, int b) {
        int res = a;

        if (b == 0) {
            a = 0;
        } else if (b > 0) {

        }
        
        return res;
    }

    @Override
    public int divide(int a, int b) {
        if (b == 0) {
            throw new ArithmeticException();
        }
        boolean resEstNegatif = false;
        int res = 0;
        if (a < 0) {
            resEstNegatif = !resEstNegatif;
            a = -a;
        }
        if (b < 0) {
            resEstNegatif = !resEstNegatif;
            b = -b;
        }
        while (a > 0) {
            a = substract(a, b);
            res++;
        }
        if (resEstNegatif) {
            res = -res;
        }
        return res;
    }

    @Override
    public int add(int a, int b) {
        int res = a;
        if (b > 0) {
            while (b != 0) {
                a++;
                b--;
            }
        } else if (b < 0) {
            while (b != 0) {
                a--;
                b++;
            }
        }
        return a;
    }

    @Override
    public int substract(int a, int b) {
        int res = a;

        if (b > 0) {
            while (b != 0) {
                a--;
                b--;
            }
        } else if (b < 0) {
            while (b != 0) {
                a++;
                b++;
            }
        }

        return a;
    }

}
